/******************
	This test provies scenario for load testing a NSI-WS simulating the traffic comming from the Data explorer:
		1.- Assess the current performance of the NSI-WS under typical and peak load.
		2.- Make sure that the NSI-WS is continuously meeting the performance standards as changes are made to the system (code and infrastructure).
	
*******************/
import http from 'k6/http';
import { check, sleep, group, fail } from 'k6';
import { initConfig, DataExplorerDataDisplay } from '../Resources/utils.js';

let INPUT_FILE = __ENV.TEST_CASES_FILE || "../Resources/test-cases-exports-de.json";

//Load test cases from json file
const TEST_CASES= JSON.parse(open(INPUT_FILE));
	
export let options = {
	systemTags: ['check','error_code','group','method','name','status'],
	//Target = number of max users to scale to
	stages: [
		{ duration: '2m', target: 40 }, // simulate ramp-up of traffic from 1 to 40 users over 2 minutes.
		{ duration: '4m', target: 40 }, // stay at 40 users for 4 minutes
		{ duration: '1m', target: 100 }, // ramp-up to 100 users over 1 minutes (peak hour starts)
		{ duration: '40s', target: 100 }, // stay at 100 users for short amount of time (peak hour)
		{ duration: '1m', target: 40 }, // ramp-down to 40 users over 1 minutes (peak hour ends)
		{ duration: '4m', target: 40 }, // continue at 40 for additional 4 minutes
		{ duration: '2m', target: 0 }, // ramp-down to 0 users
	],
	thresholds: {
		'checks': ['rate>0.99'], // more than 99% success rate
		
        "http_req_duration":   	["avg<498"],//less than 0.498 seconds
		
		'groupDuration{groupName:User landing in DF overview page}': ['avg < 5000'],
		'groupDuration{groupName:Table display - User filter on dimensions}': ['avg < 3000'],
		
        "http_req_duration{requestType:getDataflow}": 				["avg<1000"],
        "http_req_duration{requestType:getDsd}": 					["avg<1000"],
        "http_req_duration{requestType:getAvailableConstraint}": 	["avg<1000"],
        "http_req_duration{requestType:getData}": 					["avg<1000"],
		
        "http_req_duration{requestType:optionsDataflow}": 				["avg<1000"],
        "http_req_duration{requestType:optionsDsd}": 					["avg<1000"],
        "http_req_duration{requestType:optionsAvailableConstraint}": 	["avg<1000"],
        "http_req_duration{requestType:optionsData}": 					["avg<1000"],
		
        "http_req_duration{availableConstraint_datasetSize:extraSmall}": 	["avg<1000"],
        "http_req_duration{availableConstraint_datasetSize:small}": 		["avg<1000"],
        "http_req_duration{availableConstraint_datasetSize:medium}": 		["avg<1000"],
        "http_req_duration{availableConstraint_datasetSize:large}": 		["avg<1000"],
        "http_req_duration{availableConstraint_datasetSize:extraLarge}": 	["avg<1000"],
        "http_req_duration{availableConstraint_responseSize:extraSmall}": ["avg<1000"],
        "http_req_duration{availableConstraint_responseSize:small}": 		["avg<1000"],
        "http_req_duration{availableConstraint_responseSize:medium}": 	["avg<1000"],
        "http_req_duration{availableConstraint_responseSize:large}": 		["avg<1000"],
        "http_req_duration{availableConstraint_responseSize:extraLarge}": ["avg<1000"],
		
        "http_req_duration{data_datasetSize:extraSmall}": 	["avg<1000"],
        "http_req_duration{data_datasetSize:small}": 		["avg<1000"],
        "http_req_duration{data_datasetSize:medium}": 		["avg<1000"],
        "http_req_duration{data_datasetSize:large}": 		["avg<1000"],
        "http_req_duration{data_datasetSize:extraLarge}": 	["avg<1000"],
        "http_req_duration{data_responseSize:extraSmall}":["avg<1000"],
        "http_req_duration{data_responseSize:small}": 	["avg<1000"],
        "http_req_duration{data_responseSize:medium}": 	["avg<1000"],
        "http_req_duration{data_responseSize:large}": 	["avg<1000"],
        "http_req_duration{data_responseSize:extraLarge}":["avg<1000"],
	},	
	//Discard the response bodies to lessen the amount of memmory required by the testing machine.
	discardResponseBodies: true,

};

export function setup() {
	return initConfig(true);
}

export default function(config) {

	let testQuery = TEST_CASES[Math.floor(Math.random() * TEST_CASES.length)];
    
	DataExplorerDataDisplay(config, testQuery);
}
