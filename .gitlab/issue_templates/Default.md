Before opening a new ticket, please first make sure that it does not already exist in our backlog https://gitlab.com/groups/sis-cc/-/issues. You may also find an answer in our online documentation: https://sis-cc.gitlab.io/dotstatsuite-documentation/.

Please use the appropriate issue template for your specific concern in the above "Description" dropdown list. We have several templates available:

1. enhancement (Feature requests)
2. support and bug
3. documentation
4. refactoring
5. devops

Each template is designed to gather the necessary information for different types of issues. Selecting the most appropriate template will help our team address your concern more efficiently.

If you are unsure about which template to use, please use the "support and bug" template.