#!/bin/bash

#attach volume
mkdir -p /mnt/k6_sql_volume
mount -o discard,defaults,noatime /dev/disk/by-id/scsi-0DO_Volume_k6-sql-volume /mnt/k6_sql_volume

git clone https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-docker-compose.git -b develop dc

#run docker-compose & upgrade dbs
docker compose -f dc/demo/k6-bigdata-dotnet.yml -f dc/demo/docker-compose-demo-keycloak.yml up -d --quiet-pull

sleep 10s
docker ps

echo 'adding anonymous user rule'
source dc/demo/.env
docker exec mssql /opt/mssql-tools18/bin/sqlcmd -C -U $DB_SA_USER -S localhost -P $DB_SA_PASSWORD -Q "INSERT INTO ${COMMON_DB}.[dbo].[AUTHORIZATIONRULES] VALUES('*',0,'*',0,'*','*','*',3,'system',getdate())"

cd qa/PerformanceTests/oecd

PYTHONUNBUFFERED=1 locust -f oecd-performance-tests-exports.py --headless --only-summary 2>summary.txt