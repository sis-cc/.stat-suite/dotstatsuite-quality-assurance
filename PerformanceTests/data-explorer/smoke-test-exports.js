/******************
	This test provies scenario for smoke testing a NSI-WS simulating the traffic comming from the Data explorer:
		1.- smoke test provides a sanity check every time there are new changes to the NSI-WS.
		2.- Verify that your system doesn't throw any errors when under minimal load.
	
*******************/
import http from 'k6/http';
import { check, sleep, group, fail } from 'k6';
import { initConfig, DataExplorerDataDisplay } from '../Resources/utils.js';

let INPUT_FILE = __ENV.TEST_CASES_FILE || "../Resources/test-cases-exports-de.json";

//Load test cases from json file
const TEST_CASES= JSON.parse(open(INPUT_FILE));
			
export let options = {
	systemTags: ['check','error_code','group','method','name','status'],
	//Base line test 
	//Fixed number of iterations to execute the default function.
	iterations: TEST_CASES.length,
	vus: 1,  // 1 user looping 
	//duration: '1m',
	thresholds: {
		'checks': ['rate>=1.0'], // 100% success rate required
		
        "http_req_duration":   	["avg<498"],//less than 0.498 seconds
		
		'groupDuration{groupName:User landing in DF overview page}': ['avg < 5000'],
		'groupDuration{groupName:Table display - User filter on dimensions}': ['avg < 3000'],
		
        "http_req_duration{requestType:getDataflow}": 				["avg<1000"],
        "http_req_duration{requestType:getDsd}": 					["avg<1000"],
        "http_req_duration{requestType:getAvailableConstraint}": 	["avg<1000"],
        "http_req_duration{requestType:getData}": 					["avg<1000"],
		
        "http_req_duration{requestType:optionsDataflow}": 				["avg<1000"],
        "http_req_duration{requestType:optionsDsd}": 					["avg<1000"],
        "http_req_duration{requestType:optionsAvailableConstraint}": 	["avg<1000"],
        "http_req_duration{requestType:optionsData}": 					["avg<1000"],
		
        "http_req_duration{availableConstraint_datasetSize:extraSmall}": 	["avg<1000"],
        "http_req_duration{availableConstraint_datasetSize:small}": 		["avg<1000"],
        "http_req_duration{availableConstraint_datasetSize:medium}": 		["avg<1000"],
        "http_req_duration{availableConstraint_datasetSize:large}": 		["avg<1000"],
        "http_req_duration{availableConstraint_datasetSize:extraLarge}": 	["avg<1000"],
        "http_req_duration{availableConstraint_responseSize:extraSmall}": ["avg<1000"],
        "http_req_duration{availableConstraint_responseSize:small}": 		["avg<1000"],
        "http_req_duration{availableConstraint_responseSize:medium}": 	["avg<1000"],
        "http_req_duration{availableConstraint_responseSize:large}": 		["avg<1000"],
        "http_req_duration{availableConstraint_responseSize:extraLarge}": ["avg<1000"],
		
        "http_req_duration{data_datasetSize:extraSmall}": 	["avg<1000"],
        "http_req_duration{data_datasetSize:small}": 		["avg<1000"],
        "http_req_duration{data_datasetSize:medium}": 		["avg<1000"],
        "http_req_duration{data_datasetSize:large}": 		["avg<1000"],
        "http_req_duration{data_datasetSize:extraLarge}": 	["avg<1000"],
        "http_req_duration{data_responseSize:extraSmall}":["avg<1000"],
        "http_req_duration{data_responseSize:small}": 	["avg<1000"],
        "http_req_duration{data_responseSize:medium}": 	["avg<1000"],
        "http_req_duration{data_responseSize:large}": 	["avg<1000"],
        "http_req_duration{data_responseSize:extraLarge}":["avg<1000"],
		
	},	
	//Discard the response bodies to lessen the amount of memmory required by the testing machine.
	discardResponseBodies: true
};

export function setup() {
	return initConfig(true);
}

export default function(config) {

	let testQuery = TEST_CASES[__ITER];
	
	DataExplorerDataDisplay(config, testQuery);
}
