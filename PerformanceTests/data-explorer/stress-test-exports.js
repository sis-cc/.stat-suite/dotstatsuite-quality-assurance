/******************
	This test provies scenario for stress testing a NSI-WS simulating the traffic comming from the Data explorer:
		1.- Determine how your system will behave under extreme conditions.
		2.- Determine what is the maximum capacity of your system in terms of users or throughput.
		3.- Determine the breaking point of your system and its failure mode.
		4.- Determine if your system will recover without manual intervention after the stress test is over.
	
*******************/
import http from 'k6/http';
import { check, sleep, group, fail } from 'k6';
import { initConfig, DataExplorerDataDisplay } from '../Resources/utils.js';

let INPUT_FILE = __ENV.TEST_CASES_FILE || "../Resources/test-cases-exports-de.json";
	
//Load test cases from json file
const TEST_CASES= JSON.parse(open(INPUT_FILE));
		
export let options = {
	systemTags: ['check','error_code','group','method','name','status'],
	//Target = number of max users to scale to
	stages: [
		{ duration: '1m', target: 10 }, // below normal load
		{ duration: '2m', target: 10 },
		{ duration: '1m', target: 40 }, // normal load
		{ duration: '2m', target: 40 },
		{ duration: '1m', target: 70 }, // around the breaking point
		{ duration: '2m', target: 70 },
		{ duration: '1m', target: 100 }, // beyond the breaking point
		{ duration: '2m', target: 100 },
		{ duration: '3m', target: 0 }, // scale down. Recovery stage.*/
	],
	thresholds: {
		'checks': ['rate>0.99'], // more than 99% success rate
		
        "http_req_duration":   	["avg<498"],//less than 0.498 seconds
		
		'groupDuration{groupName:User landing in DF overview page}': ['avg < 5000'],
		'groupDuration{groupName:Table display - User filter on dimensions}': ['avg < 3000'],
		
        "http_req_duration{requestType:getDataflow}": 				["avg<1000"],
        "http_req_duration{requestType:getDsd}": 					["avg<1000"],
        "http_req_duration{requestType:getAvailableConstraint}": 	["avg<1000"],
        "http_req_duration{requestType:getData}": 					["avg<1000"],
		
        "http_req_duration{requestType:optionsDataflow}": 				["avg<1000"],
        "http_req_duration{requestType:optionsDsd}": 					["avg<1000"],
        "http_req_duration{requestType:optionsAvailableConstraint}": 	["avg<1000"],
        "http_req_duration{requestType:optionsData}": 					["avg<1000"],
		
        "http_req_duration{availableConstraint_datasetSize:extraSmall}": 	["avg<1000"],
        "http_req_duration{availableConstraint_datasetSize:small}": 		["avg<1000"],
        "http_req_duration{availableConstraint_datasetSize:medium}": 		["avg<1000"],
        "http_req_duration{availableConstraint_datasetSize:large}": 		["avg<1000"],
        "http_req_duration{availableConstraint_datasetSize:extraLarge}": 	["avg<1000"],
        "http_req_duration{availableConstraint_responseSize:extraSmall}": ["avg<1000"],
        "http_req_duration{availableConstraint_responseSize:small}": 		["avg<1000"],
        "http_req_duration{availableConstraint_responseSize:medium}": 	["avg<1000"],
        "http_req_duration{availableConstraint_responseSize:large}": 		["avg<1000"],
        "http_req_duration{availableConstraint_responseSize:extraLarge}": ["avg<1000"],
		
        "http_req_duration{data_datasetSize:extraSmall}": 	["avg<1000"],
        "http_req_duration{data_datasetSize:small}": 		["avg<1000"],
        "http_req_duration{data_datasetSize:medium}": 		["avg<1000"],
        "http_req_duration{data_datasetSize:large}": 		["avg<1000"],
        "http_req_duration{data_datasetSize:extraLarge}": 	["avg<1000"],
        "http_req_duration{data_responseSize:extraSmall}":["avg<1000"],
        "http_req_duration{data_responseSize:small}": 	["avg<1000"],
        "http_req_duration{data_responseSize:medium}": 	["avg<1000"],
        "http_req_duration{data_responseSize:large}": 	["avg<1000"],
        "http_req_duration{data_responseSize:extraLarge}":["avg<1000"],
	},	
	//Discard the response bodies to lessen the amount of memmory required by the testing machine.
	discardResponseBodies: true,

};

export function setup() {
	return initConfig(true);
}

export default function(config) {

	let testQuery = TEST_CASES[Math.floor(Math.random() * TEST_CASES.length)];
    
	DataExplorerDataDisplay(config, testQuery);
}
