/******************
	This test provies scenario for soak testing a NSI-WS:
		1.- The soak test uncovers performance and reliability issues stemming from a system being under pressure for an extended period.
		2.- Reliability issues typically relate to bugs, memory leaks, insufficient storage quotas, incorrect configuration or infrastructure failures. 
		3.- Performance issues typically relate to incorrect database tuning, memory leaks, resource leaks or a large amount of data.
	
*******************/
import http from 'k6/http';
import { check, sleep, group, fail } from 'k6';
import { initConfig, exportData } from './Resources/utils.js';

let INPUT_FILE = __ENV.TEST_CASES_FILE || "./Resources/test-cases-exports.json";

//Load test cases from json file
const TEST_CASES= JSON.parse(open(INPUT_FILE));
			
export let options = {
	systemTags: ['check','error_code','group','method','name','status'],
	//Target = number of max users to scale to
	/*
	-We recommend you to configure your soak test at about 80% capacity of your system. 
	-If your system can handle a maximum of 500 simultaneous users, you should configure your soak test to 400 VUs.
	*/
	stages: [
		{ duration: '2m', target: 32 }, // ramp up to 32 users
		{ duration: '50m', target: 32 }, // stay at 32 for ~50 min
		{ duration: '2m', target: 0 }, // scale down. (optional)
	],
	//Discard the response bodies to lessen the amount of memmory required by the testing machine.
	discardResponseBodies: true,
};

export function setup() {
	return initConfig(true);
}

export default function(config) {

	let testQuery = TEST_CASES[Math.floor(Math.random() * TEST_CASES.length)];
    
	exportData(config, testQuery, true);
}
