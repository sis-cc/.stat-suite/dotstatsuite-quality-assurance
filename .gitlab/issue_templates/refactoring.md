<!---

Before opening a new ticket, please first make sure that it does not already exist in our backlog https://gitlab.com/groups/sis-cc/-/issues.

--->

### What
[Briefly describe the refactoring task.]

### Why
[Explain why this refactoring is necessary.]

### How
[Outline the proposed changes and improvements.]

### What to test
[Describe how to test and validate the change(s).]

<!---

/label ~t::refactoring

--->