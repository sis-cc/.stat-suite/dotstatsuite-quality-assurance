/******************
	This test provies scenario for smoke testing data imports to the transfer-service:
		1.- Assess the current performance of the transfer-service for basic benchmark tests.
		2.- Make sure that the transfer-service is continuously meeting the performance standards as changes are made to the system (code).
	
*******************/
import http from 'k6/http';
import { check, sleep, group, fail } from 'k6';
import { Rate, Trend, Counter } from 'k6/metrics';
import { TryToGetNewAccessToken, initConfig, importData } from './Resources/utils.js';
	
let INPUT_FILE = __ENV.TEST_CASES_FILE || "./Resources/crs-import-test-cases.json";

//Load test cases from json file
const TEST_CASES= JSON.parse(open(INPUT_FILE));

//Open input files
for(let testCase in TEST_CASES){
	if(TEST_CASES[testCase].format !=="sdmx")
		TEST_CASES[testCase].data = open(`./Resources/Data/${TEST_CASES[testCase].dataFile}`, "b");
	if(TEST_CASES[testCase].format ==="excel")
		TEST_CASES[testCase].edd = open(`./Resources/Data/${TEST_CASES[testCase].eddFile}`, "b");
}

//IMPORTANT! The time has to be adjusted depending on the amount of imports, to give enough time for all the imports to complete
let waitTimeInSeconds = TEST_CASES.length * 60;//total amount of usecases * 30 seconds

let importRate = new Rate('data_import_completed');
let importTrend = new Trend('data_import_time', true);

export let options = {
	setupTimeout: "10s",
	systemTags: ['check','error_code','group','method','name','status'],
	thresholds: {
		"checks": ['rate>=1.0'], //// 100% success rate on import requests
		"data_import_completed": ['rate>=1.0'],// all imports listed in the TEST_CASES must complete successfully
		"data_import_time": ["avg<3600000"],//less than 1h		
	},
	iterations: TEST_CASES.length,
	vus: 1,
	duration: '2h'
};

export function setup() {

	return initConfig(false);	
}

export default function(config) {

	TryToGetNewAccessToken(config);
	
	let testCase = TEST_CASES[__ITER];
	
	importData(config, testCase, importRate, importTrend, 60 /* timeout in minutes */);
}
