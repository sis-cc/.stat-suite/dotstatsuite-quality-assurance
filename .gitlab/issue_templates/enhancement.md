<!---

Before opening a new ticket, please first make sure that it does not already exist in our backlog https://gitlab.com/groups/sis-cc/-/issues.

--->

**As** [your first name, or a generic user persona (the "who")],  
**I want** [explain what you want to do with the product (the "what")],  
**So that** [explain the business benefit (the "why")]. 

### Functional specifications
[Describe in details the user experience:]
- What are the specific steps or actions the user needs to take to accomplish the desired task?
- What inputs or data does the user need to provide?
- What should the user see on the screen during each step of the process?
- Are there any specific UI elements (buttons, forms, dropdowns) that need to be included?
- What feedback or confirmation should the user receive upon completing the action?
- Are there any error scenarios or edge cases that need to be addressed?
- How does this feature interact with existing functionality in the product?
- Are there any performance considerations or requirements for this feature?
- Does this feature require any specific permissions or user roles?
- Are there any accessibility requirements that need to be considered?

### Inputs
[Attach all mockups, sketches, test files (e.g., data.csv, structure.xml) that are necessary for the undertanding of the request. You can also link to live experiences you know of that include the feature you wish for.]

### Organisation using the .Stat Suite
[What is the organisation using the .Stat Suite?]

<!---

/label ~t::feature ~product-management ~s::review

--->
