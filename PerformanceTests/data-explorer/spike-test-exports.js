/******************
	This test provies scenario for spike testing a NSI-WS simulating the traffic comming from the Data explorer:
		1.- Spike test is a variation of a stress test, but it does not gradually increase the load, instead it spikes to extreme load over a very short window of time
		2.- While a stress test allows the SUT (System Under Test) to gradually scale up its infrastructure, a spike test does not.
		3.- Determine how your system will perform under a sudden surge of traffic.
		4.- Determine if your system will recover once the traffic has subsided.
	SUCCESS/FAILUER
		Excellent: system performance is not degraded during the surge of traffic. Response time is similar during low traffic and high traffic.
		Good: Response time is slower, but the system does not produce any errors. All requests are handled.
		Poor: System produces errors during the surge of traffic, but recovers to normal after the traffic subsides.
		Bad: System crashes, and does not recover after the traffic has subsided.
*******************/
import http from 'k6/http';
import { check, sleep, group, fail } from 'k6';
import { initConfig, DataExplorerDataDisplay } from '../Resources/utils.js';

let INPUT_FILE = __ENV.TEST_CASES_FILE || "../Resources/test-cases-exports-de.json";
	
//Load test cases from json file
const TEST_CASES= JSON.parse(open(INPUT_FILE));
	
export let options = {
	systemTags: ['check','error_code','group','method','name','status'],
	//Target = number of max users to scale to
	stages: [
		{ duration: '10s', target: 10 }, // below normal load
		{ duration: '1m', target: 10 },
		{ duration: '10s', target: 140 }, // spike to 140 users
		{ duration: '3m', target: 140 }, // stay at 140 for 3 minutes
		{ duration: '10s', target: 10 }, // scale down. Recovery stage.
		{ duration: '3m', target: 10 },
		{ duration: '10s', target: 0 },
	],
	
	thresholds: {
		'checks': ['rate>0.98'], // more than 98% success rate
		
        "http_req_duration":   	["avg<498"],//less than 0.498 seconds
		
		'groupDuration{groupName:User landing in DF overview page}': ['avg < 5000'],
		'groupDuration{groupName:Table display - User filter on dimensions}': ['avg < 3000'],
		
        "http_req_duration{requestType:getDataflow}": 				["avg<1000"],
        "http_req_duration{requestType:getDsd}": 					["avg<1000"],
        "http_req_duration{requestType:getAvailableConstraint}": 	["avg<1000"],
        "http_req_duration{requestType:getData}": 					["avg<1000"],
		
        "http_req_duration{requestType:optionsDataflow}": 				["avg<1000"],
        "http_req_duration{requestType:optionsDsd}": 					["avg<1000"],
        "http_req_duration{requestType:optionsAvailableConstraint}": 	["avg<1000"],
        "http_req_duration{requestType:optionsData}": 					["avg<1000"],
		
        "http_req_duration{availableConstraint_datasetSize:extraSmall}": 	["avg<1000"],
        "http_req_duration{availableConstraint_datasetSize:small}": 		["avg<1000"],
        "http_req_duration{availableConstraint_datasetSize:medium}": 		["avg<1000"],
        "http_req_duration{availableConstraint_datasetSize:large}": 		["avg<1000"],
        "http_req_duration{availableConstraint_datasetSize:extraLarge}": 	["avg<1000"],
        "http_req_duration{availableConstraint_responseSize:extraSmall}": ["avg<1000"],
        "http_req_duration{availableConstraint_responseSize:small}": 		["avg<1000"],
        "http_req_duration{availableConstraint_responseSize:medium}": 	["avg<1000"],
        "http_req_duration{availableConstraint_responseSize:large}": 		["avg<1000"],
        "http_req_duration{availableConstraint_responseSize:extraLarge}": ["avg<1000"],
		
        "http_req_duration{data_datasetSize:extraSmall}": 	["avg<1000"],
        "http_req_duration{data_datasetSize:small}": 		["avg<1000"],
        "http_req_duration{data_datasetSize:medium}": 		["avg<1000"],
        "http_req_duration{data_datasetSize:large}": 		["avg<1000"],
        "http_req_duration{data_datasetSize:extraLarge}": 	["avg<1000"],
        "http_req_duration{data_responseSize:extraSmall}":["avg<1000"],
        "http_req_duration{data_responseSize:small}": 	["avg<1000"],
        "http_req_duration{data_responseSize:medium}": 	["avg<1000"],
        "http_req_duration{data_responseSize:large}": 	["avg<1000"],
        "http_req_duration{data_responseSize:extraLarge}":["avg<1000"],
	},
		
	//Discard the response bodies to lessen the amount of memmory required by the testing machine.
	discardResponseBodies: true,

};

export function setup() {
	return initConfig(true);
}

export default function(config) {

	let testQuery = TEST_CASES[Math.floor(Math.random() * TEST_CASES.length)];
    
	DataExplorerDataDisplay(config, testQuery);
}
