import http from 'k6/http';
import { check, sleep, group, fail } from 'k6';
import { Trend } from 'k6/metrics';
let groupDuration = Trend("groupDuration");

export function initConfig(isNSI)
{
	let config = {
		accessToken:'',
		expiry:'',
		tokenUrl:__ENV.KEYCLOAK_AT_URL,
		clientId:__ENV.KEYCLOAK_CLIENT_ID || "app",
		username:__ENV.USERNAME,
		password:__ENV.PASSWORD,
		dataspace:__ENV.TRANSFER_SERVICE_DATASPACE || "stable",
		transferBaseUrl:__ENV.TRANSFER_SERVICE_HOSTNAME || "http://127.0.0.1:93",
		nsiBaseUrl:__ENV.NSIWS_HOSTNAME || "http://127.0.0.1:81"
	};
	
	let baseUrl = isNSI ? config.nsiBaseUrl : config.transferBaseUrl;
	var params = { 
		responseType: "text",
		tags:{name:'healthProbe'}
	};
	
	let healthCheck = http.get(`${baseUrl}/health`, params);
	
	if (healthCheck.status !== 200)
	{
		fail(`Error: the Service with URL {${healthCheck.request.url} is not responding.`);
	}
		
	console.log(`Testing {${baseUrl}} service, version ${healthCheck.json().service.details.version}`);
	
	return config;
}

export function importData(config, testCase, importRate, importTrend, timeOutInMinutes=10)
{
	//Submit data import requests
	let params =
	{
		headers : {
			'Accept':'application/json',
			'Authorization': `Bearer ${config.accessToken}`, 
		},
		tags:{name:'dataImport'}
	};
		
	var method = "/1.2/import/sdmxFile";
	let data= {	'dataspace': config.dataspace, 'sendEmail': 1};
	
	//Import from SDMX source
	if(testCase.format ==="sdmx" ){
		data.filepath= testCase.sdmxSource;
		//Workaround - K6 only supports multipart/from-data request if there is a file in the request. 
		data.file= http.file("", "dummyFile.csv");
		console.log(`Importing from url: ${data.filepath}`);
	}
	//Import from Excel
	else if(testCase.format ==="excel" ){
		method = "/1.2/import/excel";
		data.eddFile  = http.file(testCase.edd, testCase.eddFile);
		data.excelFile = http.file(testCase.data, testCase.dataFile);
		console.log(`Importing excel file: ${testCase.eddFile}`);
	}
	//Import from CSV and XML
	else{
		data.file = http.file(testCase.data, testCase.dataFile);
		console.log(`Importing from file: ${testCase.dataFile}`);
	}
	
	var res = http.post(
		`${config.transferBaseUrl}${method}`, 
		data, 
		params,
	);
	sleep(1);//1s

	console.log(`import status:${res.status}`);
	
	check(res, {
		'is status 200': (r) => r.status === 200
	});
	
	var date = new Date();
	var startTime =  date.getTime();
	
	if(res.status ==200){
		
		var jsonResp = res.json();
		
		console.log(`import message:${jsonResp.message}`);
		
		var transactionID = jsonResp.message.match(/\d+/g);
				
		//Wait for the transfer-service to process import
		
		if(testCase.size=="extraSmall")
			sleep(2);//2s
		else if(testCase.size=="small")
			sleep(5);//4s
		else if(testCase.size=="medium")
			sleep(20);//20s
		else if(testCase.size=="large")
			sleep(20);//20s
		else if(testCase.size=="extraLarge")
			sleep(20);//20s
		else
			sleep(10);//10s
		 
		var timeOutTime = timeOutInMinutes * 60 * 1000;
		
		do{
			//Get new access token if current token has expired
			TryToGetNewAccessToken(config);
			
			let data= { 
				'dataspace': config.dataspace,	
				'id': parseInt(transactionID)
			};
			
			params =
			{
				headers : {
					'Accept':'application/json',
					'Authorization': `Bearer ${config.accessToken}`, 
				},
				tags:{name:'statusCheck'}
			};
						
			method = "/1.2/status/request";
			var res = http.post(
				`${config.transferBaseUrl}${method}`,
				data, 
				params
			);
			sleep(1);//1s
			
		    var d = new Date();
			if(res.status===200){
				
				var jsonResp = res.json();
				
				if(jsonResp.outcome==="Success" || jsonResp.outcome==="Warning"){
					var actualTime = Date.parse(jsonResp.executionEnd) - Date.parse(jsonResp.executionStart);					
					if(Number.isNaN(actualTime) || actualTime <= 0){
						console.log(`Bad Import time:${actualTime}`);
						importRate.add(false);
					}else{
						console.log(`Transaction ID: ${transactionID}, Status: ${jsonResp.outcome}, Import time:${actualTime}`);
						importTrend.add(actualTime, { import_type: `${testCase.format}_${testCase.size}` });
						importTrend.add(actualTime, { datasetSize: `${testCase.datasetSize}` });
						//The import was completed
						importRate.add(true);
					}
					break;
				}
				else if(jsonResp.outcome==="Error" || jsonResp.executionStatus==="TimedOut" || jsonResp.executionStatus==="Canceled"){
					//The import completed with errors
					importRate.add(false);
					console.log(`Transaction ID: ${transactionID} - Execution Status: ${jsonResp.outcome}/${jsonResp.executionStatus}`);
					break;
				}
				else if(d.getTime()-startTime>=timeOutTime){
					//The import was TimedOut
					importRate.add(false);
					console.log(`timed out ${transactionID}`);
					break;
				}
			}
			sleep(10);//5s
		}while(true);
		
	}
	else{
		importRate.add(false);
		
		if(res.status >= 500){
			console.log(`import message:${res.body}`);
		}
	}
}

const SUPPORTED_RESPONSE_FORMATS =["csv", "json", "xml"];

export function exportData(config, testQuery, isRandom)
{
	TryToGetNewAccessToken(config);
	
	let params = 
	{ 
		headers: {
			'Accept-Ecoding': 'gzip, deflate',
			'Authorization': `Bearer ${config.accessToken}`
		},
		timeout: testQuery.timeout || '60s'
	};
	
	group(`Query type ${testQuery.queryType}`, function (){
		
		//structure query			
		if(testQuery.queryType==="structure"){
			
			params.headers.Accept = 'application/vnd.sdmx.structure+json; version=1.0; charset=utf-8';
			params.tags={name:'nsiStructureQuery'};
			
			group(`Struc type ${testQuery.structureType}`, function (){
				let response = http.get(
					`${config.nsiBaseUrl}${testQuery.query}`, //URL
					params, //headers 
				);	
							
				check(response, {
					"status is 200": (r) => r.status == 200
				});
			});
		}			
		//data query
		else
		{
			var randomIndex = Math.floor(Math.random() * SUPPORTED_RESPONSE_FORMATS.length);
			
			let formatsToQuery = isRandom 
				? SUPPORTED_RESPONSE_FORMATS.slice(randomIndex, randomIndex+1)
				: SUPPORTED_RESPONSE_FORMATS;
							
			for(let i in formatsToQuery)
			{								
				let responseFormat = formatsToQuery[i];
								
				params.headers.Accept=`application/vnd.sdmx.data+${responseFormat}`;
				
				if(responseFormat==="xml")
					params.headers.Accept=`application/vnd.sdmx.genericdata+${responseFormat}`;
				
				var expectedResponseStatusCode = 200;
				//has range headers
				if ('range' in testQuery) {
					params.headers.Range=testQuery.range;
					expectedResponseStatusCode = 206;//partial content
				}
				
				//add tags
				params.tags={datasetSize:testQuery.datasetSize, name:'nsiDataQuery'};
				
				group(`Format ${responseFormat}`, function (){
					group(`Size ${testQuery.responseSize}`, function (){
						let response = http.get(
							`${config.nsiBaseUrl}${testQuery.query}`, //URL
							params, //headers 
						);													
						var msg= `status is ${expectedResponseStatusCode}`;
						check(response, {
							msg: (r) => r.status == expectedResponseStatusCode
						});
						
						if(response.status !=expectedResponseStatusCode){
							console.log(`Failed request - Status: ${response.status} Query: ${testQuery.query}`);	
						}
					});
				});				
			}
		}
	});
}


export function DataExplorerDataDisplay(config, testQuery)
{
	TryToGetNewAccessToken(config);
		
	let dataflow = `${testQuery.dataflow.agency}/${testQuery.dataflow.id}/${testQuery.dataflow.version}`;
	let dsd = `${testQuery.dsd.agency}/${testQuery.dsd.id}/${testQuery.dsd.version}`;
	
	let paramsOptions = { headers: {'Accept':'*/*', 'Access-Control-Request-Method': 'GET', 'Access-Control-Request-Headers': 'authorization', 'Origin':'http:localhost'}};
	let paramsStructures = { headers: {'Accept':'application/vnd.sdmx.structure+json;version=1.0;urn=true', 'Accept-Encoding':'gzip, deflate, br, zstd', 'Accept-Language': 'en', 'Authorization': `Bearer ${config.accessToken}`}};
	let paramsOptionsData = { headers: {'Accept':'*/*','Access-Control-Request-Headers':'authorization,x-range', 'Access-Control-Request-Method': 'GET', 'Origin':'http:localhost'}};
	let paramsData = { headers: {'Accept':'application/vnd.sdmx.data+json', 'Accept-Encoding':'gzip, deflate, br, zstd', 'Accept-Language': 'en', 'X-Range':"values=0-5999", 'Authorization': `Bearer ${config.accessToken}`}};
	let unfilteredRequest = testQuery.unfilteredRequest;
	let filteredRequest = testQuery.filteredRequest;
	
	groupWithDurationMetric("User landing in DF overview page", function () {
		
		//OPTIONS Dataflow			
		let url = `${config.nsiBaseUrl}/rest/dataflow/${dataflow}?references=all&detail=referencepartial`;
		paramsOptions.tags = { requestType: 'optionsDataflow' };	
		let response = http.options(url, null, paramsOptions);				
		check(response, {"status is 200": (r) => r.status == 200});	
		if(response.status !=200){
			console.log(`Failed request OPTIONS - Status: ${response.status} - ${url}`);	
		}
		
		//GET Dataflow
		paramsStructures.tags = { requestType: 'getDataflow' };	
		response = http.get(url, paramsStructures );					
		check(response, {"status is 200": (r) => r.status == 200});	
			
		if(response.status !=200){
			console.log(`Failed request GET - Status: ${response.status} - ${url}`);
		}
		
		//OPTIONS dsd
		paramsOptions.tags = { requestType: 'optionsDsd' };
		url =`${config.nsiBaseUrl}/rest/datastructure/${dsd}?references=parents&detail=allcompletestubs`;
		response = http.options(url, null, paramsOptions);				
		check(response, {"status is 200": (r) => r.status == 200});	
		if(response.status !=200){
			console.log(`Failed request OPTIONS - Status: ${response.status} - ${url}`);	
		}
		
		//GET dsd
		paramsStructures.tags = { requestType: 'getDsd' };
		response = http.get(url, paramsStructures );					
		check(response, {"status is 200": (r) => r.status == 200});	
		if(response.status !=200){
			console.log(`Failed request GET - Status: ${response.status} - ${url}`);	
		}
		
		dataflow = `${testQuery.dataflow.agency},${testQuery.dataflow.id},${testQuery.dataflow.version}`;
		
		let availableConstraintURL =`${config.nsiBaseUrl}/rest/availableconstraint/${dataflow}/${unfilteredRequest}&mode=available`;
		let dataURL =`${config.nsiBaseUrl}/rest/data/${dataflow}/${unfilteredRequest}`;
		
		//Data & availability OPTIONS
		paramsOptions.tags = { requestType: 'optionsAvailableConstraint'};
		paramsOptionsData.tags = { requestType: 'optionsData' };
		getDataAndAvailability(config, availableConstraintURL, dataURL, paramsOptionsData, paramsOptions, true);
		
		//Data & availability  GET
		paramsStructures.tags = { requestType: 'getAvailableConstraint', 'availableConstraint_responseSize':`${testQuery.unfilteredResponseSize}`, 'availableConstraint_datasetSize':`${testQuery.datasetSize}`};
		paramsData.tags = { requestType: 'getData', 'data_responseSize':`${testQuery.unfilteredResponseSize}`, 'data_datasetSize':`${testQuery.datasetSize}`};
		getDataAndAvailability(config, availableConstraintURL, dataURL, paramsData, paramsStructures, false);
		
	});
		
	//Simulate wait for user changing to table display and apply filters
	sleep(3);//3s
	
	groupWithDurationMetric("Table display - User filter on dimensions", function () {
		let availableConstraintURL =`${config.nsiBaseUrl}/rest/availableconstraint/${dataflow}/${filteredRequest}&mode=available`;
		let dataURL =`${config.nsiBaseUrl}/rest/data/${dataflow}/${filteredRequest}`;
		
		//Data & availability  OPTIONS
		paramsOptions.tags = { requestType: 'optionsAvailableConstraint'};
		paramsOptionsData.tags = { requestType: 'optionsData' };
		getDataAndAvailability(config, availableConstraintURL, dataURL, paramsOptionsData, paramsOptions, true);
		
		//Data & availability  GET
		paramsStructures.tags = { requestType: 'getAvailableConstraint', 'availableConstraint_responseSize':`${testQuery.responseSize}`, 'availableConstraint_datasetSize':`${testQuery.datasetSize}`};
		paramsData.tags = { requestType: 'getData', 'data_responseSize':`${testQuery.responseSize}`, 'data_datasetSize':`${testQuery.datasetSize}`};
		getDataAndAvailability(config, availableConstraintURL, dataURL, paramsData, paramsStructures, false);
	});
}

function getDataAndAvailability(config, availableConstraintURL, dataURL, paramsData, paramsStructures, isOptions){
	
	let method = 'GET';
	if(isOptions){
		method = 'OPTIONS';
	}
	
	//Parallel Availableconstraint & Data
	let responses = http.batch([
		[method, availableConstraintURL, null, paramsStructures],
		[method, dataURL, null, paramsData],
	]);
		
	let dataStatus = 200;
	if(isOptions){
		check(responses[0], {
			'optionsAvailableconstraint status was 200': (res) => res.status === 200,
		});
		check(responses[1], {
			'optionsData status was 200': (res) => res.status === 200,
		});		
	}
	else{
		dataStatus = 206;
		check(responses[0], {
			'getAvailableconstraint status was 200': (res) => res.status === 200,
		});
		check(responses[1], {
			'getData status was 206': (res) => res.status === 206,
		});
	}
		
		
	if(responses[0].status !=200){
		console.log(`Failed request ${method} - Expected: 200 - Status: ${responses[0].status} - ${availableConstraintURL}`);	
	}	
	
	if(responses[1].status !=dataStatus){
		console.log(`Failed request ${method} - Expected: ${dataStatus} - Status: ${responses[1].status} - ${dataURL}`);	
	}	
}

function groupWithDurationMetric(name, group_function) {
  let start = new Date();
  group(name, group_function);
  let end = new Date();
  groupDuration.add(end - start, {groupName: name})
}

export function TryToGetNewAccessToken(config){
	
	if(!config.tokenUrl || !config.username || !config.password)
		return;
	
	if(config.accessToken && config.expiry > new Date().getTime())
		return;
	        
	let data= { 
		'grant_type': "password",
		'client_id': config.clientId, 
		'scope': "openid", 
		'username': config.username,
		'password': config.password
	};
			
	//Get new access token
	var params = {
		headers: {'Accept':'application/json'}, 
		responseType: "text",
		tags:{name:'tokenUrl'}
	};
	let res = http.post(config.tokenUrl, data, params);
	
	var responseJson = res.json();
	
	var expiryDate = new Date();
	expiryDate.setSeconds(expiryDate.getSeconds() + responseJson.expires_in);
	
	config.expiry = expiryDate.getTime();
	config.accessToken = responseJson.access_token;
}