/******************
	This test provies scenario for stress testing data imports to the transfer-service:
		1.- Determine how your system will behave under extreme conditions.
		2.- Determine what is the maximum capacity of your system in terms of users or throughput.
		3.- Determine the breaking point of your system and its failure mode.
		4.- Determine if your system will recover without manual intervention after the stress test is over.
*******************/
import http from 'k6/http';
import { check, sleep, group, fail } from 'k6';
import { Rate, Trend } from 'k6/metrics';
import { TryToGetNewAccessToken, initConfig, importData } from './Resources/utils.js';
	
let INPUT_FILE = __ENV.TEST_CASES_FILE || "./Resources/test-cases-data-imports.json";

//Load test cases from json file
const TEST_CASES= JSON.parse(open(INPUT_FILE));
//Open input files
for(let testCase in TEST_CASES){
	if(TEST_CASES[testCase].format !=="sdmx")
		TEST_CASES[testCase].data = open(`./Resources/Data/${TEST_CASES[testCase].dataFile}`, "b");
	if(TEST_CASES[testCase].format ==="excel")
		TEST_CASES[testCase].edd = open(`./Resources/Data/${TEST_CASES[testCase].eddFile}`, "b");
}

let importRate = new Rate('data_import_completed');
let importTrend = new Trend('data_import_time', true);

export let options = {
	setupTimeout: "10s",
	systemTags: ['check','error_code','group','method','name','status'],
	//Target = number of max users to scale to
	stages: [
		{ duration: '40s', target: 1 }, // below normal load
		{ duration: '90s', target: 1 },
		{ duration: '40s', target: 2 }, // normal load
		{ duration: '90s', target: 2 },
		{ duration: '40s', target: 3 }, // around the breaking point
		{ duration: '90s', target: 3 },
		{ duration: '40s', target: 5 }, // beyond the breaking point
		{ duration: '90s', target: 5 },
		{ duration: '20s', target: 1 }, // scale down. Recovery stage.
		{ duration: '4m', target: 1 }, // continue at 1 users to collect pending imports that are still being processed		
	],
	thresholds: {
		"checks": ['rate>0.99'], // more than 99% success rate on import requests
		"data_import_completed": ['rate>0.90'], // more than 90% success rate of transactions imported
        "data_import_time{import_type:csv_small}": ["avg<30000"],//less than 30 seconds
        "data_import_time{import_type:csv_medium}": ["avg<30000"],//less than 30 seconds
        "data_import_time{import_type:csv_large}": ["avg<35000"],//less than 35 seconds
        "data_import_time{import_type:xml_small}": ["avg<30000"],//less than 30 seconds
        "data_import_time{import_type:xml_medium}": ["avg<70000"],//less than  70 seconds
        "data_import_time{import_type:xml_large}": ["avg<150000"],//less than 150 seconds
        "data_import_time{import_type:sdmx_small}": ["avg<30000"],//less than 30 seconds
        "data_import_time{import_type:sdmx_medium}": ["avg<70000"],//less than  70 seconds
        "data_import_time{import_type:sdmx_large}": ["avg<150000"],//less than 150 seconds
        "data_import_time{import_type:excel_extraSmall}": ["avg<30000"],//less than 30 seconds
        "data_import_time{import_type:excel_small}": ["avg<30000"],//less than 30 seconds
        "data_import_time{import_type:excel_medium}": ["avg<70000"],//less than  70 seconds
        "data_import_time{import_type:excel_large}": ["avg<150000"],//less than 150 seconds
		
        "data_import_time{datasetSize:extraSmall}": ["avg<150000"],
        "data_import_time{datasetSize:small}": ["avg<150000"],
        "data_import_time{datasetSize:medium}": ["avg<150000"],
        "data_import_time{datasetSize:large}": ["avg<150000"],
        "data_import_time{datasetSize:extraLarge}": ["avg<150000"],
	},
	//iterations: TEST_CASES.length,
	//vus: TEST_CASES.length,

};

export function setup() {	
	return initConfig(false);	
}

export default function(config) {

	TryToGetNewAccessToken(config);
	
	let testCase = TEST_CASES[Math.floor(Math.random() * TEST_CASES.length)];
	
	importData(config, testCase, importRate, importTrend);	
}