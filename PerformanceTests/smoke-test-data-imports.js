/******************
	This test provies scenario for smoke testing data imports to the transfer-service:
		1.- Assess the current performance of the transfer-service for basic benchmark tests.
		2.- Make sure that the transfer-service is continuously meeting the performance standards as changes are made to the system (code).
	
*******************/
import http from 'k6/http';
import { check, sleep, group, fail } from 'k6';
import { Rate, Trend, Counter } from 'k6/metrics';
import { TryToGetNewAccessToken, initConfig, importData } from './Resources/utils.js';
	
let INPUT_FILE = __ENV.TEST_CASES_FILE || "./Resources/test-cases-data-imports-all_files.json";

//Load test cases from json file
const TEST_CASES= JSON.parse(open(INPUT_FILE));
//Open input files
for(let testCase in TEST_CASES){
	if(TEST_CASES[testCase].format !=="sdmx")
		TEST_CASES[testCase].data = open(`./Resources/Data/${TEST_CASES[testCase].dataFile}`, "b");
	if(TEST_CASES[testCase].format ==="excel")
		TEST_CASES[testCase].edd = open(`./Resources/Data/${TEST_CASES[testCase].eddFile}`, "b");
}

//IMPORTANT! The time has to be adjusted depending on the amount of imports, to give enough time for all the imports to complete
let waitTimeInSeconds = TEST_CASES.length * 60;//total amount of usecases * 30 seconds

let importRate = new Rate('data_import_completed');
let importTrend = new Trend('data_import_time', true);

export let options = {
	setupTimeout: "10s",
	systemTags: ['check','error_code','group','method','name','status'],
	thresholds: {
		"checks": ['rate>=1.0'], //// 100% success rate on import requests
		"data_import_completed": ['rate>=1.0'],// all imports listed in the TEST_CASES must complete successfully
		"data_import_time": ["avg<23946"],//less than 23.946 seconds
        "data_import_time{import_type:csv_small}": ["avg<10000"],//less than 10 seconds
        "data_import_time{import_type:csv_medium}": ["avg<40000"],//less than 40 seconds
        "data_import_time{import_type:csv_large}": ["avg<60000"],//less than 60 seconds
        "data_import_time{import_type:xml_small}": ["avg<10000"],//less than 10 seconds
        "data_import_time{import_type:xml_medium}": ["avg<70000"],//less than 70 seconds
        "data_import_time{import_type:xml_large}": ["avg<150000"],//less than 150 seconds
        "data_import_time{import_type:sdmx_small}": ["avg<30000"],//less than 30 seconds
        "data_import_time{import_type:sdmx_medium}": ["avg<70000"],//less than 70 seconds
        "data_import_time{import_type:sdmx_large}": ["avg<150000"],//less than 150 seconds
        "data_import_time{import_type:excel_extraSmall}": ["avg<10000"],//less than 10 seconds
        "data_import_time{import_type:excel_small}": ["avg<10000"],//less than 10 seconds
        "data_import_time{import_type:excel_medium}": ["avg<70000"],//less than 70 seconds
        "data_import_time{import_type:excel_large}": ["avg<150000"],//less than 150 seconds
		
        "data_import_time{datasetSize:extraSmall}": ["avg<150000"],
        "data_import_time{datasetSize:small}": ["avg<13522"],//less than 13.522 seconds
        "data_import_time{datasetSize:medium}": ["avg<150000"],
        "data_import_time{datasetSize:large}": ["avg<45358"],//less than 45.358 seconds
        "data_import_time{datasetSize:extraLarge}": ["avg<150000"],	
		
	},
	iterations: TEST_CASES.length,
	vus: 1,
	duration: '40m',

};

export function setup() {

	return initConfig(false);	
}

export default function(config) {

	TryToGetNewAccessToken(config);
	
	let testCase = TEST_CASES[__ITER];
	
	importData(config, testCase, importRate, importTrend);
}
