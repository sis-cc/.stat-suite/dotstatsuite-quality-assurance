<!---

Before opening a new ticket, please first make sure that it does not already exist in our backlog https://gitlab.com/groups/sis-cc/-/issues. 

--->

### Activity
- [ ] Infrastructure
- [ ] CI/CD Pipeline
- [ ] Deployment
- [ ] Monitoring
- [ ] Security
- [ ] Performance
- [ ] Other (please specify)

### What
[Briefly describe the DevSecOps task.]

### Why
[Explain why this task is necessary.]

<!---

/label ~t::DevSecOps

--->