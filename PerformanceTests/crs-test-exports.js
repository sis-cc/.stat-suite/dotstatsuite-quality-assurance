/******************
	This test provies scenario for smoke testing a NSI-WS:
		1.- smoke test provides a sanity check every time there are new changes to the NSI-WS.
		2.- Verify that your system doesn't throw any errors when under minimal load.
	
*******************/
import http from 'k6/http';
import { check, sleep, group, fail } from 'k6';
import { initConfig, exportData } from './Resources/utils.js';

let INPUT_FILE = __ENV.TEST_CASES_FILE || "./Resources/crs-export-test-cases.json";

//Load test cases from json file
const TEST_CASES= JSON.parse(open(INPUT_FILE));
			
export let options = {
	systemTags: ['check','error_code','group','method','name','status'],
	//Base line test 
	//Fixed number of iterations to execute the default function.
	iterations: TEST_CASES.length,
	vus: 1,  // 1 user looping 
	//duration: '1m',
	thresholds: {
		'checks': ['rate>=1.0'], // 100% success rate required
		
        "http_req_duration":   	["avg<60000"],
		
        "http_req_duration{group:::Query type data::Format xml::Size extraSmall}": 	["avg<60000"],
        "http_req_duration{group:::Query type data::Format xml::Size small}": 		["avg<60000"],
        "http_req_duration{group:::Query type data::Format xml::Size medium}": 		["avg<120000"],
        "http_req_duration{group:::Query type data::Format xml::Size large}": 		["avg<300000"],
        "http_req_duration{group:::Query type data::Format xml::Size extraLarge}": 	["avg<600000"],
		
        "http_req_duration{group:::Query type data::Format json::Size extraSmall}": ["avg<60000"],
        "http_req_duration{group:::Query type data::Format json::Size small}": 		["avg<60000"],
        "http_req_duration{group:::Query type data::Format json::Size medium}": 	["avg<120000"],
        "http_req_duration{group:::Query type data::Format json::Size large}": 		["avg<300000"],
        "http_req_duration{group:::Query type data::Format json::Size extraLarge}": ["avg<600000"],
		
        "http_req_duration{group:::Query type data::Format csv::Size extraSmall}": 	["avg<60000"],
        "http_req_duration{group:::Query type data::Format csv::Size small}": 		["avg<60000"],
        "http_req_duration{group:::Query type data::Format csv::Size medium}": 		["avg<120000"],
        "http_req_duration{group:::Query type data::Format csv::Size large}": 		["avg<300000"],
        "http_req_duration{group:::Query type data::Format csv::Size extraLarge}": 	["avg<600000"],
		
        "http_req_duration{group:::Query type data::Format xml::Size extraSmall_paginated}": 	["avg<60000"],
        "http_req_duration{group:::Query type data::Format xml::Size small_paginated}": 		["avg<60000"],
        "http_req_duration{group:::Query type data::Format xml::Size medium_paginated}": 		["avg<120000"],
        "http_req_duration{group:::Query type data::Format xml::Size large_paginated}": 		["avg<300000"],
        "http_req_duration{group:::Query type data::Format xml::Size extraLarge_paginated}": 	["avg<600000"],
		
        "http_req_duration{group:::Query type data::Format json::Size extraSmall_paginated}": 	["avg<60000"],
        "http_req_duration{group:::Query type data::Format json::Size small_paginated}": 		["avg<60000"],
        "http_req_duration{group:::Query type data::Format json::Size medium_paginated}": 		["avg<120000"],
        "http_req_duration{group:::Query type data::Format json::Size large_paginated}": 		["avg<300000"],
        "http_req_duration{group:::Query type data::Format json::Size extraLarge_paginated}": 	["avg<600000"],
		
        "http_req_duration{group:::Query type data::Format csv::Size extraSmall_paginated}": 	["avg<60000"],
        "http_req_duration{group:::Query type data::Format csv::Size small_paginated}": 		["avg<120000"],
        "http_req_duration{group:::Query type data::Format csv::Size medium_paginated}": 		["avg<60000"],
        "http_req_duration{group:::Query type data::Format csv::Size large_paginated}": 		["avg<300000"],
        "http_req_duration{group:::Query type data::Format csv::Size extraLarge_paginated}": 	["avg<600000"],
		
        "http_req_duration{datasetSize:extraLarge}":["avg<600000"],
        "http_req_duration{datasetSize:extraLarge_paginated}":["avg<600000"]
	},	
	//Discard the response bodies to lessen the amount of memmory required by the testing machine.
	discardResponseBodies: true
};

export function setup() {
	return initConfig(true);
}

export default function(config) {

	let testQuery = TEST_CASES[__ITER];
	
	exportData(config, testQuery, false);
}
