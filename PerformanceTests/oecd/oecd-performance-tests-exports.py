import os
import statistics
from locust import FastHttpUser, task, events
from influxdb_client import InfluxDBClient
import json
import time
import gevent

# Get input file from environment variable
input_file = os.getenv("LOCUST_INPUT_FILE", "oecd-cases-sample.json")  # Default value if not set
sdmxws_hostname = os.getenv("SDMXWS_HOSTNAME", "http://localhost")  # Default value if not set
testid = os.getenv("TESTID", "crs")
pipelineid = os.getenv("PIPELINEID", "todo")
release_name = os.getenv("RELEASE_NAME", "todo")
influxdb_token = os.getenv("INFLUXDB_TOKEN")

# Ensure the file exists
if not os.path.exists(input_file):
    print(f"Error: Input file '{input_file}' not found.")
    print("Please set the environment variable LOCUST_INPUT_FILE to a valid JSON file.")
    exit(1)

# Load IIS log data from the selected input file
with open(input_file, "r") as f:
    requests = json.load(f)

print(f"Using input file: {input_file}")

# Convert time strings to seconds (relative to the first request)
def time_to_seconds(time_str):
    h, m, s = map(float, time_str.split(":"))
    return h * 3600 + m * 60 + s

# Compute relative execution times
base_time = time_to_seconds(requests[0]["time"])
for req in requests:
    req["relative_time"] = time_to_seconds(req["time"]) - base_time

# Group requests by their relative timestamp
grouped_requests = {}
for req in requests:
    time_key = req["relative_time"]
    if time_key not in grouped_requests:
        grouped_requests[time_key] = []
    grouped_requests[time_key].append(req)

# Dictionary to store aggregated statistics by request type
type_stats = {}

class ReplayTraffic(FastHttpUser):  
    host = sdmxws_hostname
    max_connections = 1000  # Increase connection pool size
    network_timeout = 60  # Set timeout to avoid slow requests blocking execution
    connection_timeout = 3600.0  # Ensure connections are closed properly
    wait_time = lambda self: 0  # Remove artificial wait time

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.has_executed = False

    @task
    def replay_traffic(self):
        if self.has_executed:
            return

        self.has_executed = True
        start_time = time.time()
        pool = gevent.pool.Pool()
        live_probe_time = time.time()
        request_counter = 0

        for relative_time, batch in sorted(grouped_requests.items()):
            
            #report every 5 min
            if time.time() - live_probe_time >= 300:
                minutes, seconds = divmod(time.time() - start_time, 60)
                print(f"Running for {minutes:.0f}m {seconds:.0f}s, executed {request_counter} requests")
                live_probe_time = time.time()
            
            delay = max(0, relative_time - (time.time() - start_time))

            if delay > 0:
                time.sleep(delay)

            for req in batch:
                request_counter += 1
                pool.spawn(self.make_request, req)

        pool.join()
        self.environment.runner.quit()

    def make_request(self, req):
        method = req["method"]
        url = req["url"]
        headers = req.get("headers", {})
        headers["Connection"] = "keep-alive"  #Force connection reuse
        req_type = req.get("type", "unknown")
        
        if method == "OPTIONS":
            headers["Origin"] = sdmxws_hostname
            self.client.request(method, url, headers=headers)
            return

        # do we need a lock here and everywhere we use gloabal var ?
        if req_type not in type_stats:
            type_stats[req_type] = {
                'count': 0,
                'total_response_time': 0,
                'response_times': [],
                'failures': 0,
                'min_response_time': float('inf'),
                'max_response_time': float('-inf')
            }

        start_time = time.time()

        try:    
            type_stats[req_type]['count'] += 1

            response = self.client.request(method, url, headers=headers)
            
            if response.status_code not in [200, 204, 206, 404]:
                type_stats[req_type]['failures'] += 1

            response_time = (time.time() - start_time) * 1000    
                     
            type_stats[req_type]['total_response_time'] += response_time
            type_stats[req_type]['response_times'].append(response_time)

            type_stats[req_type]['min_response_time'] = min(type_stats[req_type]['min_response_time'], response_time)
            type_stats[req_type]['max_response_time'] = max(type_stats[req_type]['max_response_time'], response_time)

        except Exception as e:
            print(f"Request failed with exception: {e}, {url}")
            type_stats[req_type]['failures'] += 1

def write_to_influxdb(token, request_type, fields):
    client = InfluxDBClient(url="http://178.62.228.39:8086", token=token, org="OECD")

    measurement = {
        "measurement": testid,
        "tags": {
            "request_type": request_type,
            "pipelineid": pipelineid,
            "release": release_name,
            "dbtype": "mssql"
        },
        "fields": fields
    }

    write_api = client.write_api()
    write_api.write(bucket="OECD-DATASETS", record=measurement)
    write_api.close()

@events.quitting.add_listener
def report_type_stats(environment, **kwargs):
    
    print("\nAggregated Statistics by Request Type:")
    # print(json.dumps(type_stats, indent = 4))


    for req_type, stats in type_stats.items():
        
        print(f"Type: {req_type}")

        total_requests = stats['count']
        failed_requests = stats['failures']
        response_times = stats['response_times']
        
        fields = {
            "total_requests": total_requests,
            "failed_requests": failed_requests,
            "mean_response_time": sum(response_times) / total_requests if total_requests > 0 else 0,
            "median_response_time": statistics.median(response_times) if response_times else 0,
            "p95_response_time": statistics.quantiles(response_times, n=20)[18] if sum(response_times) > 2 else 0,
            "min_response_time": stats['min_response_time'] if total_requests > 0 else 0,
            "max_response_time": stats['max_response_time'] if total_requests > 0 else 0,
            "failure_rate": (failed_requests / total_requests) * 100 if total_requests > 0 else 0
        }
        
        print(f"  Total Requests: {total_requests}, Failed: {failed_requests}, Success: {total_requests - failed_requests}")
        print(f"  Average Response Time: {fields['mean_response_time']:.2f} ms")
        print(f"  Median Response Time: {fields['median_response_time']:.2f} ms")
        print(f"  95th Percentile Response Time: {fields['p95_response_time']:.2f} ms")
        print(f"  Min Response Time: {fields['min_response_time']:.2f} ms")
        print(f"  Max Response Time: {fields['max_response_time']:.2f} ms")
        print(f"  Failure Rate: {fields['failure_rate']:.2f}%")
        print()

        if influxdb_token:
            write_to_influxdb(influxdb_token, req_type, fields)
        
        # set exit code to 0 not to fail pipeline, todo: return exit_code based on needed threshold
        environment.process_exit_code = 0
